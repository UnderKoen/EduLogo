/**
 * Created by Under_Koen on 10/09/2018.
 */
module edulogo.display {
    requires edulogo.core;
    requires javafx.base;
    requires javafx.graphics;
    requires javafx.controls;

    exports nl.edulogo.display;
    exports nl.edulogo.display.fx;
}