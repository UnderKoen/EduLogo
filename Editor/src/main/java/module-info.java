/**
 * Created by Under_Koen on 11/09/2018.
 */
module edulogo.editor {
    requires edulogo.core;
    requires edulogo.display;
    requires javafx.base;
    requires javafx.graphics;
    requires javafx.controls;

    exports nl.edulogo.editor;
    exports nl.edulogo.editor.fx;
}